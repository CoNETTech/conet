![http protocol](/resources/CoNET_icon.png?raw=true)

[![Gitter](https://img.shields.io/badge/chat-on%20gitter-blue.svg)](https://gitter.im/QTGate/Lobby)
[![npm version](https://badge.fury.io/js/conet.svg)](https://badge.fury.io/js/conet)
[![Build Status](https://travis-ci.org/QTGate/CoNET.svg?branch=master)](https://travis-ci.org/QTGate/CoNET)
[![Coverage Status](https://coveralls.io/repos/github/QTGate/CoNET/badge.svg?branch=master)](https://coveralls.io/github/QTGate/CoNET?branch=master)
[![Known Vulnerabilities](https://snyk.io/test/github/qtgate/conet/badge.svg?targetFile=package.json)](https://snyk.io/test/github/qtgate/conet?targetFile=package.json)

![http protocol](/resources/conet1.png?raw=true)

### CoNET is global network that allow devices connected each other without IP address

CoNET project create a new Internet that provide devices have Anonymous, Stealth, Secure communication.


### CoNETはIPアドレスなしで複数のデバイスを相互接続した情報通信網であります

CoNETは新インターネットという【匿名】【隱身】【セキュア】な通信ネットワークであります

### CoNET是無IP地址讓設備相互連結的互聯網

CoNET項目旨在創建一個全新的互聯網，用戶在其中可以【匿名】【隱身】【安全】的相互通訊

![http protocol](/resources/conet2.png?raw=true)

### Node and endpoint

Node in CoNET provider for connectivity and exchange points, gateway for Internet.

CoNET official provide a node that connect address is: QTGate@CoNETTech.ca

### ノードとエンドポイント

CoNETのノードはISPのようなエンドポイントに接続サービスします。ノード間にデータのエクスチェンジ、エンドポイントの発見、インターネットのゲットウェイサービスです

CoNET公式ノード接続アドレスは: QTGate@CoNETTech.ca

### 節點和通訊端點

在CoNET中節點提供設備接入服務,節點之間提供數據交換和路由信息,傳統互聯網的代理

CoNET提供一個官方節點，它的接入地址是：QTGate@CoNETTech.ca

### [CoNET Platform, CoNETプラットフォーム, CoNET平台](https://github.com/QTGate/QTGate-Desktop-Client)
- [CoNET Platform for laptop, CoNETプラットフォーム for PC, CoNET計算機OS平台](https://github.com/QTGate/QTGate-Desktop-Client)
- CoNET Platform for iOS (come soon), CoNETプラットフォーム for iOS (開発中), iOS的CoNET平台開發中
- [CoNET Platform for Android, CoNETプラットフォーム for Android, 安卓系統的CoNET平台](https://github.com/QTGate/CoNETPlatform-Android)

![http protocol](/resources/CoPlatform3.png?raw=true)

### Donate us
Email: info@CoNETTech.ca